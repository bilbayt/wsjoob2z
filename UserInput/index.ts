export class TextInput {
    // TODO: implement TextInput and NumericInput to pass unit tests

    public add(c) {
        throw new Error("Not Implemented");
    }

    public getValue() {
        throw new Error("Not Implemented");
    }
}

export class NumericInput extends TextInput { }

export function userInput() {
    const input = new NumericInput();

    input.add("1");
    input.add("a");
    input.add("0");

    console.log(input.getValue());
}
