import { getMatrixElementsSum } from ".";

describe("Matrix", () => {
    test("matrix sum returns 9", () => {
        const input = [
            [0, 1, 1, 2],
            [0, 5, 0, 0],
            [2, 0, 3, 3],
        ];

        const actual = getMatrixElementsSum(input);
        const expected = 9;

        expect(actual).toBe(expected);
    });

    test("matrix sum returns 9", () => {
        const input = [
            [1, 1, 1, 0],
            [0, 5, 0, 1],
            [2, 1, 3, 10],
        ];

        const actual = getMatrixElementsSum(input);
        const expected = 9;

        expect(actual).toBe(expected);
    });

    test("matrix sum returns 18", () => {
        const input = [
            [1, 1, 1],
            [2, 2, 2],
            [3, 3, 3],
        ];

        const actual = getMatrixElementsSum(input);
        const expected = 18;

        expect(actual).toBe(expected);
    });

    test("matrix sum returns 0", () => {
        const input = [[0]];

        const actual = getMatrixElementsSum(input);
        const expected = 0;

        expect(actual).toBe(expected);
    });
});
