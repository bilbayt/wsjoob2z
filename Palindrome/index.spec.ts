import { isPalindrome } from ".";

describe("Palindrome", () => {
    test("isPalindrome null input throws exception", () => {
        expect(() => isPalindrome(null)).toThrowError("Invalid Input");
    });

    test("isPalindrome empty input throws exception", () => {
        expect(() => isPalindrome("")).toThrow("Invalid Input");
    });

    test("isPalindrome single case palindrome returns true", () => {
        const actual = isPalindrome("racecar");
        expect(actual).toBeTruthy();
    });

    test("isPalindrome mixed case palindrome returns true", () => {
        const actual = isPalindrome("RACEcar");
        expect(actual).toBeTruthy();
    });

    test("isPalindrome non palindrome returns false", () => {
        const actual = isPalindrome("firetruck");
        expect(actual).toBeFalsy();
    });
});
