# Bilbayt Code Tests

Please complete these tests by implementing various methods within this project. Tests are complete when all unit tests (jest) pass and you are happy with the code you've written.

HINT: You can run the jest unit tests from command line:

```
npm test
```

HINT: To quickly find where you need to write code you can look search for "// TODO" in the project.

## TypeScript

### 1. Palindrome

A palindrome is a word that reads the same backward or forward.

Write a function that checks if a given word is a palindrome. Character case should be ignored.

For example, isPalindrome("Racecar") should return true as character case should be ignored, resulting in "racecar", which is a palindrome since it reads the same backward and forward.

Location | Unit Tests
--- | ---
Palindrome/index.ts | Palindrome/index.spec.ts

### 2. User Input

User interface contains two types of user input controls: **TextInput**, which accepts all characters and **NumericInput**, which accepts only digits.

Implement the class **TextInput** that contains:

* Public method _add(c)_ - adds the given character to the current value
* Public method _getValue()_ - returns the current value

Implement the class **NumericInput** that:

* Extends **TextInput**
* Overrides the _add(c)_ method so that each non-numeric character is ignored

For example, the following code should output "10":

```
const input = new NumericInput();
input.Add('1');
input.Add('a');
input.Add('0');

console.log(input.getValue());
```

Location | Unit Tests
--- | ---
UserInput/index.ts | UserInput/index.spec.ts

### 3. Matrix Elements Sum

After they became famous, the CodeBots all decided to move to a new building and live together. The building is represented by a rectangular `matrix` of rooms. Each cell in the `matrix` contains an integer that represents the price of the room. Some rooms are _free_ (their cost is `0`), but that's probably because they are haunted, so all the bots are afraid of them. That is why any room that is _free_ or is located *anywhere below* a _free_ room in the same column are not considered suitable for the bots to live in.

Help the bots calculate the total price of all the rooms that are suitable for them.

**Example**

For:

```
matrix = [[0, 1, 1, 2],
          [0, 5, 0, 0],
          [2, 0, 3, 3]]
```

The output should be:

```
matrixElementsSum(matrix) = 9
```

Here's the rooms matrix with unsuitable rooms marked with an `x`:

```
matrix = [[x, 1, 1, 2],
          [x, 5, x, x],
          [x, x, x, x]]
```

Thus, the answer is `1 + 5 + 1 + 2 = 9`

For:

```
matrix = [[1, 1, 1, 0],
          [0, 5, 0, 1],
          [2, 1, 3, 10]]
```

The output should be:

```
matrixElementsSum(matrix) = 9
```

Here's the rooms matrix with unsuitable rooms marked with an `x`:

```
matrix = [[1, 1, 1, x],
          [x, 5, x, x],
          [x, 1, x, x]]
```

Note that the free room in the first row makes the full column unsuitable for bots.

Thus, the answer is `1 + 1 + 1 + 5 + 1 = 9`

Location | Unit Tests
--- | ---
Matrix/index.ts | Matrix/index.spec.ts
