import { NumericInput, TextInput } from ".";

describe("UserInputs", () => {
    test("TextInput add various characters returns correct value", () => {
        const textInput = new TextInput();

        textInput.add("1");
        textInput.add("a");
        textInput.add("0");

        const actual = textInput.getValue();
        const expected = "1a0";

        expect(actual).toBe(expected);
    });

    test("NumericInput add various characters returns correct value", () => {
        const textInput = new NumericInput();

        textInput.add("1");
        textInput.add("0");

        const actual = textInput.getValue();
        const expected = "10";

        expect(actual).toBe(expected);
    });

    test("NumericInput add various characters returns correct value", () => {
        const textInput = new NumericInput();

        textInput.add("1");
        textInput.add("a");
        textInput.add("0");

        const actual = textInput.getValue();
        const expected = "10";

        expect(actual).toBe(expected);
    });
});
